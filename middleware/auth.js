export default function ({ store, route, redirect }) {
  const email = localStorage.email || null
  if(email!==null){
    store.commit('setLogin', {email, loggedIn:true})
  }else{
    store.commit('setLogin', {email:'', loggedIn:false})
  }
  if (!store.state.email) {
    if (route.path !== '/') redirect('/')
  }
  if (store.state.email) {
    if (route.path === '/') redirect('/to-do')
  }
}
