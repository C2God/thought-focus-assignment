export const state = () => ({
  loggedIn: false,
  email: '',
  primaryColor: '#1976d2',
  todoList: [
    {
      id: 1,
      name: 'Take over world',
      selected: false,
    },
    {
      id: 2,
      name: 'hello',
      selected: true,
    },
    {
      id: 3,
      name: 'Gym',
      selected: false,
    },
  ],
  activeList: [],
  completedList: [],
})

export const getters = {
  loggedIn: (state) => state.loggedIn,
  email: (state) => state.email,
}

export const actions = {
  login({ commit }, data) {
    return new Promise((resolve, reject) => {
      if (data.email === 'test@test.com' && data.password === '123456') {
        localStorage.setItem('email', data.email)
        commit('setLogin', { email: data.email, loggedIn: true })
        resolve(data)
      } else {
        reject(new Error('INVALID_DETAILS'))
      }
    })
  },
  logOut({ commit }) {
    localStorage.removeItem('email')
    commit('setLogin', { email: '', loggedIn: false })
  },
  addTodo({ commit }, data) {
    commit('addTodo', data)
  },
  removeTodo({ commit }, data) {
    commit('removeTodo', data)
  },
  toggleSelect({ commit }, checkAll) {
    commit('toggleSelect', checkAll)
  },
  completeTodo({ commit }, data) {
    commit('completeTodo', data)
    commit('getCompletedList')
    commit('getActiveList')
  },
  getAllList({ state }) {
    return state.todoList
  },
  getActiveList({ commit }) {
    commit('getActiveList')
  },
  getCompletedList({ commit }) {
    commit('getCompletedList')
  },
  clearCompleted({ commit }) {
    commit('clearCompleted')
  },
}

export const mutations = {
  setLogin(state, data) {
    state.loggedIn = data.loggedIn
    state.email = data.email
  },
  addTodo(state, data) {
    state.todoList = [...state.todoList, data]
  },
  removeTodo(state, data) {
    state.todoList = state.todoList.filter((ele) => ele.id !== data.id)
    state.activeList = state.todoList.filter((ele) => ele.id !== data.id)
    state.completedList = state.todoList.filter((ele) => ele.id !== data.id)
  },
  toggleSelect(state, data) {
    state.todoList = state.todoList.map((ele) => ({ ...ele, selected: data }))
  },
  completeTodo(state, data) {
    state.todoList = state.todoList.map((ele) => {
      // const item = state.todoList.find((ele) => ele.id === data.id)
      if (ele.id === data.id) {
        return { ...ele, ...{ selected: data.selected } }
      } else {
        return ele
      }
    })
  },
  getActiveList(state) {
    state.activeList = state.todoList.filter((ele) => !ele.selected)
  },
  getCompletedList(state) {
    state.completedList = state.todoList.filter((ele) => ele.selected)
  },
  clearCompleted(state) {
    state.completedList = []
    state.todoList = state.todoList.filter((ele) => !ele.selected)
  },
}
