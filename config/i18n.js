import en from '../locales/en.json'
import hi from '../locales/hi'
export default {
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en, hi }
}